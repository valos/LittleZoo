/**
 * Little Zoo
 *
 * written by Valéry Febvre
 * vfebvre@easter-eggs.com
 *
 * Copyright 2015-2017 Valéry Febvre
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var app;

var $$ = Dom7;

var photos = [];
var sounds = [];
var settings;
var sound;
var soundLock = false;

navigator.mozL10n.once(function() {
    // load media info
    $$.getJSON('media_enhanced.json', function(data) {
        var i, j;
        var html;
        var index;
        var item;

        for(i=0; i<data.length / 2; i++) {
            html = '<div class="card">';
            html += '<div class="card-content">';
            html += '<div class="row">';

            for(j=0; j<2; j++) {
                index = i * 2 + j;
                item = data[index];

                photos.push({
                    url: 'media/' + item.photo.filename,
                    caption: item.photo.description,
                    source: item.photo.source,
                    author: item.photo.author,
                    link: item.photo.url,
                    license: item.photo.license
                });
                html += '<div class="col-50">';
                html += '<div data-index="' + index + '" data-background="media/' + item.photo.filename + '" class="card-photo lazy lazy-fadein"></div>';
                html += '<div class="card-content-inner uppercase">';
                html += navigator.mozL10n.get(item.name);
                html += '</div>';
                html += '</div>';
            }

            html += '</div>';
            html += '</div>';
            html += '</div>';

            $$('.view-main .page-content').append(html);

            for(j=0; j<2; j++) {
                item = data[i * 2 + j];
                sounds.push({
                    url: 'media/' + item.sound.filename,
                    source: item.sound.source,
                    author: item.sound.author,
                    link: item.sound.url,
                    license: item.sound.license
                });
            }
        }

        app = new Framework7({
            material: true
        });

        app.addView('.view-main', {
            domCache: true
        });

        var playSound = function(swiper) {
            if (soundLock) {
                return;
            }

            var index = swiper.activeIndex;
            if (index === 0) {
                index = sounds.length;
            }
            if (index > sounds.length) {
                index = 1;
            }

            sound = new buzz.sound(sounds[index - 1].url);
            sound.bind("ended", function(e) {
                soundLock = false;
                sound = null;
            });

            soundLock = true;
            sound.play();
        };

        var stopSound = function() {
            if (sound) {
                sound.stop();
                sound = null;
                soundLock = false;
            }
        };

        // create photo browser
        var photoBrowser = app.photoBrowser({
            photos : photos,
            loop: true,
            zoom: true,
            exposition: false,
            swipeToClose: false,
            type: 'standalone',
            theme: 'dark',
            ofText: navigator.mozL10n.get('of'),
            onOpen: function(pb) {
                pb.swiper.on('destroy', function(swiper) {
                    stopSound();
                });
                pb.swiper.on('slideChangeStart', function(swiper) {
                    stopSound();

                    // auto play animal sound if defined in settings
                    if (settings && settings.autoplay.length) {
                        playSound(swiper);
                    }
                });
                pb.swiper.on('tap', function(swiper, event) {
                    playSound(swiper);
                });

                settings = app.formGetData('settings');

                if (settings && settings.autoplay.length) {
                    playSound(pb.swiper);
                }

                // hide animal description if defined in settings
                if (settings && settings.hide_description.length) {
                    $$('.photo-browser-captions').hide();
                }

                // enable/disable ability to zoom photos
                pb.swiper.zoom.destroy();
                if (settings && settings.enable_zoom.length) {
                    pb.swiper.zoom.init()
                }
            }
        });

        // add event on card to open photo browser when user click on an animal
        $$('.card-photo').on('click', function() {
            photoBrowser.open($$(this).data('index'));
        });

        app.onPageInit('credits', function(page) {
            var html;

            $$.each(photos, function(i, photo) {
                var sound = sounds[i];
                html = '<li>';
                html += '<div class="item-content">';
                html += '<div class="item-media"><img src="' + photo.url + '" width="80"></div>';

                html += '<div class="item-inner">';
                html += '<div>';
                html += '<a href="javascript:void(0);" onclick="return openLink(\'' + photo.link + '\');" class="external">';
                html += '<div class="item-title color-white">' + navigator.mozL10n.get('photo') + '</div>';
                html += '<div class="item-subtitle">' + photo.author + '</div>';
                html += '<div class="item-text">' + photo.source + '</div>';
                html += '<div class="item-text">' + photo.license + '</div>';
                html += '</a>';
                html += '</div>';

                html += '<div style="padding-top: 8px">';
                html += '<a href="javascript:void(0);" onclick="return openLink(\'' + sound.link + '\');" class="external">';
                html += '<div class="item-title color-white">' + navigator.mozL10n.get('sound') + '</div>';
                html += '<div class="item-subtitle">' + sound.author + '</div>';
                html += '<div class="item-text">' + sound.source + '</div>';
                html += '<div class="item-text">' + sound.license + '</div>';
                html += '</a>';
                html += '</div>';
                html += '</div>';

                html += '</div>';
                html += '</li>';

                $$(page.container).find('#media-list ul').append(html);
            });
        });
    });
});

function openLink(url) {
    window.open(url, '_blank', 'location=yes');
    return false;
}
