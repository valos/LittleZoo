Little Zoo
======

Litte web app (PWA) to discover animals and learn their sound.

Little Zoo is the ideal app to discover animals and learn their sound. It is perfect for any age from one to five.

Learn and spend good time together with your baby or toddler, or let it discover a world of animals by itself. Simple, yet endlessly enjoyable, Little Zoo lets your child unlock the magic of learning.

With just a single tap, your child can have fun learning all animals sounds. Find out what your child's favourite animal is today!


Licensing
===
 * LittleZoo → GNU Affero GPL v3.0
 * Framework7 → MIT
 * Buzz! → MIT

Install
===
```
$ ./media_downloader.py media_downloader.cfg
$ npm install
$ rm app/sw.js; gulp generate-service-worker
```
