'use strict';

var gulp = require('gulp');

gulp.task('generate-service-worker', function(callback) {
  var path = require('path');
  var swPrecache = require('sw-precache');
  var rootDir = 'app';

  swPrecache.write(path.join(rootDir, 'sw.js'), {
    staticFileGlobs: [rootDir + '/**/*.{js,html,css,png,ini,properties,jpg,ogg,json}'],
    stripPrefix: rootDir,
    replacePrefix: '.'
  }, callback);
});
