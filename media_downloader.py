#! /usr/bin/env python

import freesound
import json
from optparse import OptionParser
import os
import requests
import subprocess
import shlex
import shutil
import sys
import urllib2

from flask import Flask
from flask import abort
from flask import redirect
from flask import request
from flask import url_for

media_dir = 'app/media'

CODECS_EXTENSIONS = dict(
    vorbis='.ogg',
    pcm_s16be='.aiff',
    pcm_s16le='.wav',
    pcm_s24le='.wav',
    flac='.flac',
    mp3='.mp3',
)

app = Flask(__name__)

def change_filename_extension(filename, extension):
    return '.'.join(filename.split('.')[:-1]) + extension

def wget_output_file(name, media):
    filename = media['url'].split('/')[-1]
    ext = filename.split('.')[-1]
    return '{0}-src.{1}'.format(name, ext)


@app.route('/')
def index():
    return '<a href="start">Start download of Little Zoo media (photos and sounds)</a>'


@app.route('/start')
def start():
    url = 'https://www.freesound.org/apiv2/oauth2/authorize/?client_id={0}&response_type=code&state=xyz'.format(
        app.config['CLIENT_ID'])
    return redirect(url)


@app.route('/oauth_callback')
def oauth_callback():
    code = request.args.get('code', '')
    params = dict(
        client_id=app.config['CLIENT_ID'],
        client_secret=app.config['CLIENT_SECRET'],
        grant_type='authorization_code',
        code=code,
    )
    r = requests.post('https://www.freesound.org/apiv2/oauth2/access_token/', data=params)
    data = r.json()
    access_token = data['access_token']

    download(access_token)

    return 'Download of media completed'


def download(access_token):
    if not os.path.exists(media_dir):
        os.mkdir(media_dir)
    os.chdir(media_dir)

    with open('../../media.json') as media_json_file:
        media = json.load(media_json_file)
        media_dict = dict()
        for item in media:
            media_dict[item['name']] = item

    new_media_enhanced = []
    if os.path.exists('../media_enhanced.json'):
        with open('../media_enhanced.json') as media_enhanced_json_file:
            media_enhanced = json.load(media_enhanced_json_file)
            media_enhanced_dict = dict()
            for item in media_enhanced:
                media_enhanced_dict[item['name']] = item
    else:
        media_enhanced = []
        media_enhanced_dict = dict()

    c = freesound.FreesoundClient()
    c.set_token(access_token, 'oauth')

    for item in media:
        if item['name'] in media_enhanced_dict:
            item_enhanced = media_enhanced_dict[item['name']]
        else:
            item_enhanced = dict(
                name=item['name']
            )

        # photo
        photo = item['photo']
        filename = '{0}.jpg'.format(item['name'])
        if not os.path.exists(filename):
            filename_src = wget_output_file(item['name'], photo)
            os.system('wget -O {0} {1}'.format(filename_src, photo['url']))

            cmd = u'convert -strip -quality 75 -interlace Plane'
            if photo.get('crop-w') and photo.get('crop-h'):
                cmd += u' -crop {0}x{1}+{2}+{3}'.format(photo['crop-w'], photo['crop-h'], photo['crop-x'], photo['crop-y'])
            if photo.get('resize'):
                cmd += u' -resize {0}'.format(photo['resize'])
            cmd += u' {0} {1}'.format(filename_src, filename)

            print cmd
            os.system(cmd.encode('utf-8'))
            os.remove(filename_src.encode('utf-8'))

            item_enhanced['photo'] = dict(
                filename=filename,
                source=photo['source'],
                description=photo['description'],
                author=photo['author'],
                license=photo['license'],
                url=photo['url_page'],
            )

        # sound
        sound = item['sound']
        filename = '{0}.ogg'.format(item['name'])
        if not os.path.exists(filename):
            convert_to_ogg = False
            item_enhanced['sound'] = dict()

            if sound.get('freesound_id'):
                f_sound = c.get_sound(sound['freesound_id'])

                filename_src = '{0}.{1}'.format(f_sound.id, f_sound.type)
                f_sound.retrieve('.', filename_src)

                item_enhanced['sound']['source'] = 'Freesound'
                item_enhanced['sound']['license'] = f_sound.license
                item_enhanced['sound']['author'] = f_sound.username
                item_enhanced['sound']['url'] = f_sound.url

                if f_sound.type != 'ogg':
                    convert_to_ogg = True
            else:
                filename_src = wget_output_file(item['name'], sound)
                if sound.get('url_method') == 'POST':
                    r = requests.post(sound['url'], data=sound['url_data'])
                    with open(filename_src, 'wb') as fd:
                        fd.write(r.content)
                else:
                    os.system('wget -O {0} {1}'.format(filename_src, sound['url']))

                if sound['type'] == 'video':
                    # get codec name from audio stream info
                    cmd = 'ffprobe -v 0 -print_format json -show_streams -select_streams a ' + filename_src
                    stream = subprocess.check_output(shlex.split(cmd))
                    codec_name = json.loads(stream)['streams'][0]['codec_name']

                    if codec_name in CODECS_EXTENSIONS:
                        filename_src_new = change_filename_extension(filename_src, CODECS_EXTENSIONS[codec_name])
                        if codec_name != 'vorbis':
                            convert_to_ogg = True
                    else:
                        print 'Unknown codec {0}: file {1}'.format(codec_name, filename_src)
                        sys.exit(1)

                    # remove video stream
                    os.system('ffmpeg -v 24 -i {0} -vn -acodec copy {1}'.format(filename_src, filename_src_new))
                    os.remove(filename_src)
                    filename_src = filename_src_new
                else:
                    if not filename_src.endswith('ogg'):
                        convert_to_ogg = True

                item_enhanced['sound']['source'] = sound['source']
                item_enhanced['sound']['license'] = sound['license']
                item_enhanced['sound']['author'] = sound['author']
                item_enhanced['sound']['url'] = sound['url_page']

            if convert_to_ogg or (sound.get('cut-seek') and sound.get('cut-time')):
                if convert_to_ogg:
                    filename_src_new = change_filename_extension(filename_src, '.ogg')
                    try:
                        cmd = 'oggenc -q 3 -o {0} {1}'.format(filename_src_new, filename_src)
                        subprocess.check_output(shlex.split(cmd))
                    except:
                        os.system('ffmpeg -v 24 -i {0} {1}'.format(filename_src, filename_src_new))
                    os.remove(filename_src)
                    filename_src = filename_src_new
                if sound.get('cut-seek') and sound.get('cut-time'):
                    cmd = 'ffmpeg -v 24 '
                    cmd += ' -ss {0} -i {1} -t {2}'.format(sound['cut-seek'], filename_src, sound['cut-time'])
                    cmd += ' {0}'.format(filename)
                else:
                    cmd = 'cp {0} {1}'.format(filename_src, filename)
            else:
                cmd = 'cp {0} {1}'.format(filename_src, filename)

            item_enhanced['sound']['filename'] = filename

            print cmd
            os.system(cmd.encode('utf-8'))
            os.remove(filename_src.encode('utf-8'))

        new_media_enhanced.append(item_enhanced)

    os.chdir('..')
    f = open('media_enhanced.json', 'w+')
    f.write(json.dumps(new_media_enhanced))
    f.close()


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = OptionParser(u'usage: %prog <config_uri>')

    options, arguments = parser.parse_args(argv)
    if len(arguments) != 1:
        parser.error(u'Missing configuration file.')
    config_uri = arguments[0]

    app.config.from_pyfile(config_uri)

    app.run(port=5000)


if __name__ == '__main__':
    sys.exit(not main())
